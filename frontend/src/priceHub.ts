import { HubConnectionBuilder, LogLevel } from '@aspnet/signalr'
export default {
  install: (app, options) => {
    app.config.globalProperties.$hub = new Object();

    const connection = new HubConnectionBuilder()
      .withUrl('https://localhost:7189/pricehub')
      .configureLogging(LogLevel.Information)
      .build()

    connection.start()

    connection.on('PriceChanged', (productId, price) => {
      app.config.globalProperties.$hub.priceChanged(productId, price);
    })
  }
}