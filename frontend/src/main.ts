import { createApp } from 'vue'
import App from './App.vue'
import PriceHub from './priceHub'

import './assets/main.css'

const app = createApp(App)

app.use(PriceHub)

app.mount('#app')