﻿using GbdCodeChallenge.Domain.Enum;

namespace GbdCodeChallenge.Domain;

public class Product: BaseEntity
{
    public string Name { get; set; }
    public decimal Price { get; set; }
    public ProductStatus Status { get; set; }
    public int Stock { get; set; }

    public void PutOnShelves()
    {
        Status = ProductStatus.Normal;
    }

    public void PullOffShelves()
    {
        Status = ProductStatus.SoldOut;
    }
}
