﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GbdCodeChallenge.Domain.Enum;
public enum ProductStatus
{
    Normal,
    SoldOut
}
