﻿using GbdCodeChallenge.Domain;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata;

namespace GbdCodeChallenge.Infrastructure.Database;

public class ShopDbContext : DbContext
{
    public DbSet<Product> Product { get; set; }

    public ShopDbContext(DbContextOptions options) : base(options)
    {

    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(ShopDbContext).Assembly);
        SeedData(modelBuilder);
    }

    private static void SeedData(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Product>().HasData(new Product { Name = "MacBook", Price = 99.9M, Stock = 100 });
        modelBuilder.Entity<Product>().HasData(new Product { Name = "Surface book", Price = 100M, Stock = 50 });
    }
}