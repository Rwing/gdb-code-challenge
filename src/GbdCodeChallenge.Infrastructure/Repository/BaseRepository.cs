﻿using GbdCodeChallenge.Domain;
using GbdCodeChallenge.Infrastructure.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace GbdCodeChallenge.Infrastructure.Repository;

public class BaseRepository<T> : IRepository<T> where T : BaseEntity
{
    private readonly ShopDbContext _shopDbContext;

    public BaseRepository(ShopDbContext shopDbContext)
    {
        _shopDbContext = shopDbContext;
    }

    public async Task CreateAsync(T entity)
    {
        await _shopDbContext.Set<T>().AddAsync(entity);
    }

    public Task UpdateAsync(T entity)
    {
        T exist = _shopDbContext.Set<T>().Find(entity.Id);
        _shopDbContext.Entry(exist).CurrentValues.SetValues(entity);
        return Task.CompletedTask;
    }

    public Task RemoveAsync(T entity)
    {
        _shopDbContext.Set<T>().Remove(entity);
        return Task.CompletedTask;
    }

    public async Task<T> GetByIdAsync(Guid entityId, CancellationToken cancellationToken = default)
    {
        return await _shopDbContext.Set<T>().FindAsync(entityId);
    }

    public async Task<List<T>> GetListAsync(Expression<Func<T, bool>> expression = null, bool noTracking = false, CancellationToken cancellationToken = default)
    {
        IQueryable<T> query = _shopDbContext.Set<T>();
        if (noTracking) query = query.AsNoTracking();
        if (expression != null) query = query.Where(expression);
        return await query.ToListAsync(cancellationToken);
    }

    public Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        return _shopDbContext.SaveChangesAsync(cancellationToken);
    }
}
