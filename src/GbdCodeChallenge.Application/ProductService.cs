﻿using GbdCodeChallenge.Domain;
using GbdCodeChallenge.Infrastructure.Repository;

namespace GbdCodeChallenge.Application;

public class ProductService : IProductService
{
    private readonly IRepository<Product> _productRepository;

    public ProductService(IRepository<Product> productRepository)
    {
        _productRepository = productRepository;
    }

    public async Task<List<Product>> GetAllProducts()
    {
        var entities = await _productRepository.GetListAsync();
        return entities;
    }

    public async Task Create(Product input)
    {
        await _productRepository.CreateAsync(input);
        await _productRepository.SaveChangesAsync();
    }

    public async Task PutOnShelves(Guid productId)
    {
        var product = await _productRepository.GetByIdAsync(productId);
        if (product == null)
            throw new Exception("product not found");

        product.PutOnShelves();
        await _productRepository.UpdateAsync(product);
        await _productRepository.SaveChangesAsync();
    }

    public async Task PullOffShelves(Guid productId)
    {
        var product = await _productRepository.GetByIdAsync(productId);
        if (product == null)
            throw new Exception("product not found");

        product.PullOffShelves();
        await _productRepository.UpdateAsync(product);
        await _productRepository.SaveChangesAsync();
    }

    public async Task ChangePrice(Guid productId, decimal price)
    {
        var product = await _productRepository.GetByIdAsync(productId);
        if (product == null)
            throw new Exception("product not found");
        if (price <= 0)
            throw new Exception("the price can't less than or equal to 0");

        product.Price = price;
        await _productRepository.UpdateAsync(product);
        await _productRepository.SaveChangesAsync();
    }
}
