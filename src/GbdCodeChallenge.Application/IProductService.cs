﻿using GbdCodeChallenge.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GbdCodeChallenge.Application;

public interface IProductService
{
    Task<List<Product>> GetAllProducts();
    Task Create(Product input);
    Task PutOnShelves(Guid productId);
    Task PullOffShelves(Guid productId);
    Task ChangePrice(Guid productId, decimal price);
}
