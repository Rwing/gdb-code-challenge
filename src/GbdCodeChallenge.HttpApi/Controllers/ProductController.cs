using GbdCodeChallenge.Application;
using GbdCodeChallenge.Domain;
using GbdCodeChallenge.HttpApi.Hubs;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace GbdCodeChallenge.HttpApi.Controllers;

[ApiController]
[Route("[controller]")]
public class ProductController : ControllerBase
{
    private readonly IProductService _productService;
    private readonly IHubContext<PriceHub, IPriceHub> _priceHub;

    public ProductController(IProductService productService, IHubContext<PriceHub, IPriceHub> priceHub)
    {
        _productService = productService;
        _priceHub = priceHub;
    }

    [HttpGet]
    public Task<List<Product>> Get()
    {
        return _productService.GetAllProducts();
    }

    [HttpPost]
    public async Task Post(Product input)
    {
        await _productService.Create(input);
    }

    [HttpPut]
    [Route("[action]")]
    public async Task PutOnShelves(Guid productId)
    {
        await _productService.PutOnShelves(productId);
    }

    [HttpPut]
    [Route("[action]")]
    public async Task PullOffShelves(Guid productId)
    {
        await _productService.PullOffShelves(productId);
    }

    [HttpPut]
    [Route("[action]")]
    public async Task ChangePrice(Guid productId, decimal price)
    {
        await _productService.ChangePrice(productId, price);
        await _priceHub
            .Clients
            .All
            .PriceChanged(productId,price);
    }
}
