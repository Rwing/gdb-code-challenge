﻿using Microsoft.AspNetCore.SignalR;

namespace GbdCodeChallenge.HttpApi.Hubs;

public interface IPriceHub
{
    Task PriceChanged(Guid productId, decimal price);
}

public class PriceHub : Hub<IPriceHub>
{
    
}
