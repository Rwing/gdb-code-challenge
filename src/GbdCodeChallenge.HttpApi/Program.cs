using GbdCodeChallenge.Application;
using GbdCodeChallenge.HttpApi.Hubs;
using GbdCodeChallenge.Infrastructure.Database;
using GbdCodeChallenge.Infrastructure.Repository;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Create services to the container.

builder.Services.AddControllers();
builder.Services.AddSignalR();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors(options =>
{
    options.AddPolicy("cors",
        policy =>
        {
            policy.WithOrigins("http://localhost:5173", "http://127.0.0.1:5173")
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials();
        });
});

builder.Services.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));
builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddDbContext<ShopDbContext>(options =>
                options.UseInMemoryDatabase("ShopDb"));


var app = builder.Build();

DatabaseEnsureCreated(app);

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseCors("cors");
app.UseAuthorization();

app.MapControllers();
app.MapHub<PriceHub>("/priceHub");

app.Run();

void DatabaseEnsureCreated(WebApplication webApplication)
{
    var serviceScopeFactory = webApplication.Services.GetRequiredService<IServiceScopeFactory>();
    using (var serviceScope = serviceScopeFactory.CreateScope())
    {
        var dbContext = serviceScope.ServiceProvider.GetService<ShopDbContext>();
        dbContext.Database.EnsureCreated();
    }
}
