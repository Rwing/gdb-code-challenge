using GbdCodeChallenge.Domain.Enum;
using Shouldly;

namespace GbdCodeChallenge.Domain.Test;

public class Product_Test
{
    [Fact]
    public void Should_PutOnShelves_Status_Success()
    {
        // arrange
        var domain = new Product();

        // act
        domain.PutOnShelves();

        // assert
        domain.Status.ShouldBe(ProductStatus.Normal);
    }

    [Fact]
    public void Should_PullOffShelves_Status_Success()
    {
        // arrange
        var domain = new Product();

        // act
        domain.PullOffShelves();

        // assert
        domain.Status.ShouldBe(ProductStatus.SoldOut);
    }
}