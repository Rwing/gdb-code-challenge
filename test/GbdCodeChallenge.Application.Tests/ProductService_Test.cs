using GbdCodeChallenge.Domain;
using GbdCodeChallenge.Domain.Enum;
using GbdCodeChallenge.Infrastructure.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace GbdCodeChallenge.Application.Tests;

public class ProductService_Test : TestBase
{
    public ProductService_Test()
    {
        SeedData();
    }

    private async Task SeedData()
    {
        var repo = ServiceProvider.GetService<IRepository<Product>>();
        if ((await repo.GetListAsync()).Count == 0)
        {
            await repo.CreateAsync(new Product()
            { Id = Guid.Parse("e989b56e-67f1-4d65-ad0f-3fdca38cf364"), Name = "test 1" });
            await repo.CreateAsync(new Product() { Name = "test 2" });
            await repo.CreateAsync(new Product() { Name = "test 3" });
            await repo.SaveChangesAsync();
        }
    }

    [Fact]
    public async void Should_Create_Product_Success()
    {
        // arrange
        var productRepository = ServiceProvider.GetService<IRepository<Product>>();
        var service = new ProductService(productRepository);
        var product = new Product() { Name = "test" };

        // act
        await service.Create(product);

        // assert
        var products = await service.GetAllProducts();
        products.Count.ShouldBe(4);
        product.Id.ShouldNotBe(Guid.Empty);
        products.Last().Name.ShouldBe("test");
    }

    [Fact]
    public async void Should_Get_All_Products()
    {
        // arrange
        var productRepository = ServiceProvider.GetService<IRepository<Product>>();
        var service = new ProductService(productRepository);

        // act
        var products = await service.GetAllProducts();

        // assert
        products.Count.ShouldBe(3);
        products.Any(i => i.Id == Guid.Parse("e989b56e-67f1-4d65-ad0f-3fdca38cf364")).ShouldBeTrue();
    }

    [Fact]
    public async void Should_PutOnShelves_Success()
    {
        // arrange
        var productRepository = ServiceProvider.GetService<IRepository<Product>>();
        var service = new ProductService(productRepository);

        // act
        await service.PutOnShelves(Guid.Parse("e989b56e-67f1-4d65-ad0f-3fdca38cf364"));

        // assert
        var product = await productRepository.GetByIdAsync(Guid.Parse("e989b56e-67f1-4d65-ad0f-3fdca38cf364"));
        product.Status.ShouldBe(ProductStatus.Normal);
    }

    [Fact]
    public async void Should_Exception_When_Error_ProductId_PutOnShelves()
    {
        // arrange
        var productRepository = ServiceProvider.GetService<IRepository<Product>>();
        var service = new ProductService(productRepository);

        // act
        var exception = await Assert.ThrowsAsync<Exception>(async () =>
        {
            await service.PutOnShelves(new Guid());
        });

        // assert
        exception.Message.ShouldBe("product not found");
    }

    [Fact]
    public async void Should_PullOffShelves_Success()
    {
        // arrange
        var productRepository = ServiceProvider.GetService<IRepository<Product>>();
        var service = new ProductService(productRepository);

        // act
        await service.PullOffShelves(Guid.Parse("e989b56e-67f1-4d65-ad0f-3fdca38cf364"));

        // assert
        var product = await productRepository.GetByIdAsync(Guid.Parse("e989b56e-67f1-4d65-ad0f-3fdca38cf364"));
        product.Status.ShouldBe(ProductStatus.SoldOut);
    }

    [Fact]
    public async void Should_Exception_When_Error_ProductId_PullOffShelves()
    {
        // arrange
        var productRepository = ServiceProvider.GetService<IRepository<Product>>();
        var service = new ProductService(productRepository);

        // act
        var exception = await Assert.ThrowsAsync<Exception>(async () =>
        {
            await service.PullOffShelves(new Guid());
        });

        // assert
        exception.Message.ShouldBe("product not found");
    }

    [Fact]
    public async void Should_ChangePrice_Success()
    {
        // arrange
        var productRepository = ServiceProvider.GetService<IRepository<Product>>();
        var service = new ProductService(productRepository);

        // act
        await service.ChangePrice(Guid.Parse("e989b56e-67f1-4d65-ad0f-3fdca38cf364"), 1.11M);

        // assert
        var product = await productRepository.GetByIdAsync(Guid.Parse("e989b56e-67f1-4d65-ad0f-3fdca38cf364"));
        product.Price.ShouldBe(1.11M);
    }
}