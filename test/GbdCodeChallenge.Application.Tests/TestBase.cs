﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GbdCodeChallenge.Infrastructure.Database;
using GbdCodeChallenge.Infrastructure.Repository;
using Microsoft.EntityFrameworkCore;

namespace GbdCodeChallenge.Application.Tests;

public class TestBase
{
    protected IServiceProvider ServiceProvider { get; set; }
    public TestBase()
    {
        var services = new ServiceCollection();
        services.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));
        services.AddScoped<IProductService, ProductService>();
        services.AddDbContext<ShopDbContext>(options =>
            options.UseInMemoryDatabase("ShopDb"));

        ServiceProvider = services.BuildServiceProvider();
    }
}
